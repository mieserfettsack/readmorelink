# readmorelink

This TYPO3 extensions adds a read more link to content elements. The editor can decide to add a link at the bottom of any 
content element which has the header_link field. The text of the link can be changed directly inside the content element 
via constants.

## Installation

This extension only works with TYPO3. Pick one of the following ways to install this extension to your TYPO3 page.

### Install via composer

`composer require mdy/readmorelink`

### Install via TYPO3 Extension Manager

Search for readmorelink and install the extension. Make sure the extension is activated after installation.

### Install a release from version history

Go to https://extensions.typo3.org/extension/readmorelink/#version-history and choose one of the releases available.
Download the archive, extract it and upload it to your TYPO3 via FTP, SSH or whatever.

## Configuration

### Add extension to your website

Go to the module **Template** and click on your root page of your website inside the pagetree.

Make sure that **Info/Modify** is selected in the drop down menu. Click on **Edit the whole template record** go to the
tab **Includes**. Choose **TypoScript of EXT:readmorelink (readmorelink)** from the list **Available Items** and add it
to the list **Selected Items**.

### Change default text inside read more link

Use the **Constant Editor** or add a constant with the text you want like this:

`plugin.mdy.readmorelink.strings.readmore = Another Text`

## Development and testing

[ddev](https://github.com/drud/ddev) is required! You can easily develop and test the extension using ddev on your local
machine. Git clone the extension and run the following command:

`ddev create`

This will create a fresh installation of TYPO3 10 with readmorelink installed and ready to test. Point your browser
to `https://readmorelink.ddev.site` when the script is done.

You can run acceptance tests locally with following command:

`ddev tests`