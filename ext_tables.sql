#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
  readmorelinktext varchar(255) DEFAULT '' NOT NULL,
  readmorelinkactive tinyint(3) DEFAULT '0' NOT NULL,
  readmoredisableheaderlink tinyint(3) DEFAULT '0' NOT NULL
);