<?php
defined('TYPO3_MODE') or die();

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

$additionalColumns = [
    'readmorelinkactive' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:readmorelink/Resources/Private/Language/locallang.xlf:plugin.mdy.readmorelink.be.string.activate.label',
        'config' => [
            'type' => 'check',
        ],
    ],
    'readmorelinktext' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:readmorelink/Resources/Private/Language/locallang.xlf:plugin.mdy.readmorelink.be.string.override.label',
        'config' => [
            'type' => 'input',
            'size' => '120',
            'eval' => 'trim',
        ],
    ],
    'readmoredisableheaderlink' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:readmorelink/Resources/Private/Language/locallang.xlf:plugin.mdy.readmorelink.be.string.readmoredisableheaderlink.label',
        'config' => [
            'type' => 'check',
        ],
    ]
];

ExtensionManagementUtility::addTCAcolumns('tt_content', $additionalColumns);

$GLOBALS['TCA']['tt_content']['palettes']['headers']['showitem'] = '
    header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel,
    --linebreak--,
    header_layout;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_layout_formlabel,
    header_position;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_position_formlabel,
    date;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:date_formlabel,
    --linebreak--,
    header_link;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel,
    readmorelinkactive;LLL:EXT:readmorelink/Resources/Private/Language/locallang.xlf:plugin.mdy.readmorelink.be.string.activate.label,
    readmorelinktext;LLL:EXT:readmorelink/Resources/Private/Language/locallang.xlf:plugin.mdy.readmorelink.be.string.override.label,
    readmoredisableheaderlink;LLL:EXT:readmorelink/Resources/Private/Language/locallang.xlf:plugin.mdy.readmorelink.be.string.readmoredisableheaderlink.label,
    --linebreak--,
    subheader;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:subheader_formlabel
';