Feature('Read more link frontend tests');

Scenario('Precache and test availability of test pages', (I) => {
    I.amOnPage('http://readmorelink.ddev.site/nothing-selected');
    I.amOnPage('http://readmorelink.ddev.site/header-link-and-nothing-else-selected');
    I.amOnPage('http://readmorelink.ddev.site/header-link-and-read-more-link-activated');
    I.amOnPage('http://readmorelink.ddev.site/header-link-read-more-link-and-default-text-overwritten-via-typoscript');
    I.amOnPage('http://readmorelink.ddev.site/header-link-read-more-link-activated-and-overwritten-text');
});

Scenario('Default FSC rendering', (I) => {
    I.amOnPage('http://readmorelink.ddev.site/nothing-selected');
    I.checkFSCMarkup(false);
});

Scenario('Headerlink is set', (I) => {
    I.amOnPage('http://readmorelink.ddev.site/header-link-and-nothing-else-selected');
    I.checkFSCMarkup(true);
});

Scenario('Headerlink is set and read more link activated', (I) => {
    I.amOnPage('http://readmorelink.ddev.site/header-link-and-read-more-link-activated');
    I.checkFSCMarkup(true);
    I.see('Read more','.frame-read-more-link-item');
});

Scenario('Headerlink is set, read more link activated and text is overwritten by constants', (I) => {
    I.amOnPage('http://readmorelink.ddev.site/header-link-read-more-link-and-default-text-overwritten-via-typoscript');
    I.checkFSCMarkup(true);
    I.see('Overwrite default read more text via constants','.frame-read-more-link-item');
});

Scenario('Headerlink is set, read more link activated and text is overwritten by content', (I) => {
    I.amOnPage('http://readmorelink.ddev.site/header-link-read-more-link-activated-and-overwritten-text');
    I.checkFSCMarkup(true);
    I.see('Override read more link text from content element','.frame-read-more-link-item');
});

Scenario('No Headerlink is set but read more link', (I) => {
    I.amOnPage('http://readmorelink.ddev.site/no-header-link-but-read-more-link');
    I.checkFSCMarkup(false);
    I.see('Read more','.frame-read-more-link-item');
    I.dontSeeElementInDOM('header > h2 > a');
});
