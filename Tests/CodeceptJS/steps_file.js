// in this file you can append custom step methods to 'I' object

module.exports = function() {
  return actor({
    checkFSCMarkup: function (link=false) {
      I = this;

      I.see('Header', 'h2');
      I.see('Subheader', 'h3');
      I.see('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '.ce-bodytext');
      if (link) {
        I.seeAttributesOnElements('h2 a', { href: "https://typo3.org/"});
      }
    }
  })
};
